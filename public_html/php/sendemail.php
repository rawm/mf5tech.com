<?php
	if($_POST['submit']){
		
		if(!$_POST['subject']){
			$error='Please fill in subject';
		}
		if(!$_POST['emailAddress']){
			$error.='<br />Please enter email address';
		}
		
	}
	if($error){
		$result='<div class="alert alert-danger">'.$error.'</div>';
	}else{
		if(mail($_POST['emailAddress'], $_POST['subject'], $_POST['from'], $_POST['subject'])){
			
			$result='<div class="alert alert-success">Submitted</div>';
		}else{
			echo 'message not sent';
		}
	}
		

?>
<!doctype html>

<html>

<head>
	<title>POST Example</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<style>
	#submitButton{
		margin-top:50px;
		
	}
	
	#submitButton:hover{
		background-color:green;
	}
	</style>
</head>

<body>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

	
	<div class='container'>
		<h1>
			Send Email with PHP
		</h1>
		<?php echo '<br />'.$result; ?>
	</div>

	<div class='container' id='emailDiv'>
		

		<div class='row'>
			<div class='col-md-6 col-md-offset-3'>
				<form method='post'>
					<div class='form-group'>
						<label for='emailAddress'>Email Address</label>
						<input class='form-control' type='email' name='emailAddress' id='emailAddressBox' />
					</div>
					<div class='form-group'>
						<label for='subject'>Subject</label>
						<input class='form-control' type='text' name='subject' />
					</div>

					<div class='form-group'>
						<label for='from'>From</label>
						<input class='form-control' type='text' name='from' />
					</div>

					<div class='form-group'>
						<textarea class='form-control' name='textArea'>Text</textarea>
						<input class='form-control' type="submit" name='submit' id='submitButton' />
					</div>

				</form>
			</div>

		</div>

	</div>

</body>

</html>