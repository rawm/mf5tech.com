<!doctype html>

<html>
	<title>POST Example</title>
	<style>
		#nameBox{
			border-radius:10px;
		}
	</style>
</html>
<body>
	
	<?php 
		$names=array('raymond','jeremiah', 'megan', 'micah','brandy' );
		if($_POST['submit']){
			if($_POST['name']){
				foreach($names as $key => $value){
					if($_POST['name'] == $value){
						echo "Hello ".$value;
					}
					
				}
			
			}
		}else{
			echo 'Please enter your name..';
		}
	?>
	
	<form method='post'>
		<label for='name'>NAME</label>
		<input type='text' name='name' id='nameBox'/>
		<input type="submit" name='submit' />
		
	</form>
	
</body>